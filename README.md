# README #

An example of an app using the Payfirma API to process payments for charitable donations.

### Notes for Consideration ###

* Written in Swift, but some of it has to be ObjC-backed instead of pure since my intentions to inject a data source through IB requires ObjC interoperability. This is my first time writing an application in Swift, and perhaps there is a better form of Dependency Injection out of the box but I haven't yet heard of it.
* Technically, it might make more sense if each charity would have its own Payfirma ID, in collaboration with this application. Currently donations to all charities would share the same single sandboxed ID. This would then require an assumption the payments are routed through some financial organization running iPhilanthropist, to use charity codes and route the money to the charities.
* There is a hook to read back the URL called from the HQ app, and it is even covered by unit tests, but there wasn't enough time to implement something to do with the return URL. The idea was to add 'Payment History' functionality, and have the history persist between application runs using CoreData.

### Screenshots (Looks best on iPhone 6) ###

![charities_01.png](https://bitbucket.org/repo/XbpXd9/images/3601381026-charities_01.png)

![charities_02.png](https://bitbucket.org/repo/XbpXd9/images/396037997-charities_02.png)

![charities_03.png](https://bitbucket.org/repo/XbpXd9/images/3530928253-charities_03.png)