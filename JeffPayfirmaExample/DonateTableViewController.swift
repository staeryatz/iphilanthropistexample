//
//  DonateTableViewController.swift
//  JeffPayfirmaExample
//
//  Created by Jeffrey Bakker on 2015-07-21.
//  Copyright (c) 2015 Jeffrey Bakker. All rights reserved.
//

import UIKit

class DonateTableViewController:
    UITableViewController, AmountChangedListener {

    let blueIosColor = UIColor(red: 0.0, green: 0.478431, blue: 1.0, alpha: 1.0)

    var donateTappedDelegate : DonateTappedListener?

    var charity: CharityInfo?
    var amount: String = "$25"

    func setCharityInfo(info : CharityInfo) {
        charity = info
    }

    func setDonateTappedListener(listener: DonateTappedListener) {
        donateTappedDelegate = listener
    }
    
    func amountChanged(newAmount: String) {
        amount = newAmount
    }

    func donateTapped(sender: UIButton!) {
        donateTappedDelegate?.donateTapped(charity!.name, amount: amount)
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {

        if indexPath.section == 0 {
            switch (indexPath.row) {
            case 0:
                return populateNameCell()
            case 1:
                return populateCauseCell()
            case 2:
                return populateInfoCell()
            default:
                return UITableViewCell()
            }
        }
        else if indexPath.section == 1 {
            return populateAmountCell()
        }
        else if indexPath.section == 2 {
            return populateDonateCell()
        }

        return UITableViewCell()
    }

    func populateNameCell() -> UITableViewCell {

        let cell = getDetailCell("charityNameCell")
        cell.textLabel?.text = charity?.name
        return cell
    }

    func populateCauseCell() -> UITableViewCell {

        let cell = getDetailCell("charityCauseCell")
        cell.textLabel?.text = "Causes"
        cell.detailTextLabel?.text = charity?.cause
        return cell
    }

    func populateInfoCell() -> UITableViewCell {

        let cell = getTextViewCell("charityInfoCell")
        var textView : UITextView = (cell.subviews.first as? UITextView)!
        textView.text = charity?.info
        return cell
    }

    func populateAmountCell() -> UITableViewCell {

        let cell = getTextFieldCell("donationAmount")
        return cell
    }

    func populateDonateCell() -> UITableViewCell {

        let cell = getButtonCell("donateNow")
        return cell
    }

    func getDetailCell(cellId: String) -> UITableViewCell {
        if let testCell = tableView.dequeueReusableCellWithIdentifier(cellId) as? UITableViewCell {
            return testCell
        } else {
            return UITableViewCell(style: UITableViewCellStyle.Value1, reuseIdentifier: cellId)
        }
    }

    func getTextViewCell(cellId: String) -> UITableViewCell {
        if let testCell = tableView.dequeueReusableCellWithIdentifier(cellId) as? UITableViewCell {
            return testCell
        } else {
            var cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: cellId)
            var textFrame = CGRect(x: 0, y: 0, width: 380, height: 100)
            var textView = UITextView(frame: textFrame)
            cell.insertSubview(textView, atIndex: 0)
            return cell
        }
    }

    func getTextFieldCell(cellId: String) -> UITableViewCell {
        if let testCell = tableView.dequeueReusableCellWithIdentifier(cellId) as? UITableViewCell {
            return testCell
        } else {
            var cell = AmountTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: cellId)
            cell.amountChangedDelegate = self
            return cell
        }
    }

    func getButtonCell(cellId: String) -> UITableViewCell {
        if let testCell = tableView.dequeueReusableCellWithIdentifier(cellId) as? UITableViewCell {
            return testCell
        } else {
            var cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: cellId)
            var buttonFrame = CGRect(x: 0, y: 0, width: 380, height: 44)
            var button = UIButton(frame: buttonFrame)
            button.setTitle("Donate Now", forState: UIControlState.Normal)
            button.backgroundColor = blueIosColor
            button.addTarget(self, action: "donateTapped:", forControlEvents: .TouchUpInside)
            cell.contentView.addSubview(button)
            return cell
        }
    }
}
