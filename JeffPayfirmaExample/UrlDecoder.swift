//
//  UrlDecoder.swift
//  JeffPayfirmaExample
//
//  Created by Jeffrey Bakker on 2015-07-21.
//  Copyright (c) 2015 Jeffrey Bakker. All rights reserved.
//

import Foundation

class UrlDecoder: NSObject {

    // Covered by unit tests:
    //  happy path - parse out 3 key/value pairs
    //  nil object - request value for non-existant key
    //  no pairs - url contains no query
    static func decodeUrl(url : NSURL) -> [String: String] {

        let query : String? = url.query
        var pairs = [String: String]()

        // Don't completely die if URL doesn't contain a query
        if query == nil {
            return pairs
        }

        let queryPairs : [String]? = query?.componentsSeparatedByString("&")

        for queryPair in queryPairs! {
            let bits : [String] = queryPair.componentsSeparatedByString("=")
            if bits.count != 2 {
                continue
            }

            let key : String? = bits.first?.stringByReplacingPercentEscapesUsingEncoding(NSUTF8StringEncoding)
            let value : String? = bits.last?.stringByReplacingPercentEscapesUsingEncoding(NSUTF8StringEncoding)

            pairs[key!] = value!
        }

        return pairs
    }
}
