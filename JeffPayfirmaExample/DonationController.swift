//
//  DonationController.swift
//  JeffPayfirmaExample
//
//  Created by Jeffrey Bakker on 2015-07-21.
//  Copyright (c) 2015 Jeffrey Bakker. All rights reserved.
//

import UIKit

class DonationController: NSObject, DonateTappedListener {

    @IBOutlet var donateView: DonateTableViewController!

    override func awakeFromNib() {
        donateView.setDonateTappedListener(self)
    }

    func  donateTapped(charity: String, amount: String) {
        processDonation(charity, amount: amount)
    }

    func processDonation(charity: String, amount: String) {

        let dollarAmount = amount.stringByReplacingOccurrencesOfString("$", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)

        let payfirmaHqUrl = UrlGenerator.generateUrl(charity, amount: dollarAmount)
        UIApplication.sharedApplication().openURL(NSURL(string: payfirmaHqUrl)!)
    }
}
