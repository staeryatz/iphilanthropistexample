//
//  UrlGenerator.swift
//  JeffPayfirmaExample
//
//  Created by Jeffrey Bakker on 2015-07-21.
//  Copyright (c) 2015 Jeffrey Bakker. All rights reserved.
//

import Foundation

class UrlGenerator: NSObject {

    static func generateUrl(charity: String, amount: String) -> String {

        let baseUrl = "payfirmahq://process"
        let charge = "amount=\(amount)"
        let description = "description=\(charity)"
        let transactionType = "transaction_type=SALE"
        let returnUrl = "return_url=donate://openURL"
        let email = "email=\(getCannedUserEmail())"

        let paymentUrl = "\(baseUrl)?\(charge)&\(transactionType)&\(description)&\(returnUrl)&\(email)".stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding) as String!

        return paymentUrl
    }

    static func getCannedUserEmail() -> String {

        // This would ideally be saved from the user during first-run setup
        // or in some app settings page, and then retrieved somwhere else
        // then be injected into the URL generator. For NOW it is canned.
        return "canned@user.email"
    }
}
