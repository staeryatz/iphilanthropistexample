//
//  CharitiesTableViewController.swift
//  JeffPayfirmaExample
//
//  Created by Jeffrey Bakker on 2015-07-21.
//  Copyright (c) 2015 Jeffrey Bakker. All rights reserved.
//

import UIKit

class CharitiesTableViewController: UITableViewController {

    @IBOutlet
    var charitiesData : CharitiesModel!

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {

        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return charitiesData.getAvailableCharitiesCount()
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("charityInfoCell", forIndexPath: indexPath) as! UITableViewCell

        // Populate the cell with charity information
        let charity = charitiesData.getCharityInfo(indexPath.row)
        cell.textLabel?.text = charity.name
        cell.detailTextLabel?.text = charity.cause

        return cell
    }

    // MARK: - Navigation

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {

        if (segue.identifier! == "makeDonation") {
            let path = tableView.indexPathForSelectedRow()
            let charity = charitiesData.getCharityInfo(path!.row)
            let donateTvc = segue.destinationViewController as! DonateTableViewController
            donateTvc.setCharityInfo(charity)
        }
    }
}
