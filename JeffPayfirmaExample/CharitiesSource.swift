//
//  CharitiesSource.swift
//  JeffPayfirmaExample
//
//  Created by Jeffrey Bakker on 2015-07-21.
//  Copyright (c) 2015 Jeffrey Bakker. All rights reserved.
//

import Foundation

// This is a canned implementation of the CharitiesModel abstraction.
// An actual implementation might involve using CoreData local storage
// or REST web services in order to populate the data dynamically.
class CharitiesSource: NSObject, CharitiesModel {

    var charitiesData : [CharityInfo] = []

    override init() {

        super.init()

        // Start canned data entry
        let charity1 = CharityInfo()
        charity1.name = "United Way"
        charity1.info = "To strengthen our community’s capacity to address social issues."
        charity1.cause = "local, community"

        let charity2 = CharityInfo()
        charity2.name = "Unicef"
        charity2.info = "At UNICEF, we believe that every child, regardless of circumstances or socioeconomic background, has the right to grow and thrive."
        charity2.cause = "children"

        let charity3 = CharityInfo()
        charity3.name = "Salvation Army"
        charity3.info = "The Salvation Army exists to share the love of Jesus Christ, meet human needs and be a transforming influence in the communities of our world."
        charity3.cause = "global, communities"

        let charity4 = CharityInfo()
        charity4.name = "BC Cancer Foundation"
        charity4.info = "The BC Cancer Foundation is the fundraising partner of the BC Cancer Agency and the largest funder of cancer research in this province."
        charity4.cause = "cancer"

        let charity5 = CharityInfo()
        charity5.name = "Canadian Mental Health Association"
        charity5.info = "As the nation-wide leader and champion for mental health, CMHA helps people access the resources they need to maintain and improve mental health and community integration, build resilience, and support recovery from mental illness."
        charity5.cause = "mental health"

        charitiesData.append(charity1)
        charitiesData.append(charity2)
        charitiesData.append(charity3)
        charitiesData.append(charity4)
        charitiesData.append(charity5)
        // End canned data entry
    }

    @objc func getAvailableCharitiesCount() -> Int {
        return charitiesData.count
    }

    @objc func getCharityInfo(index : Int) -> CharityInfo {
        return charitiesData[index]
    }
}
