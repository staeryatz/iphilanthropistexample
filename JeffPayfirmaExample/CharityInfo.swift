//
//  CharityInfo.swift
//  JeffPayfirmaExample
//
//  Created by Jeffrey Bakker on 2015-07-21.
//  Copyright (c) 2015 Jeffrey Bakker. All rights reserved.
//

import Foundation

@objc class CharityInfo {

    var name: String = ""
    var info: String = ""
    var cause: String = ""
}
