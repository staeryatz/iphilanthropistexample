//
//  CharitiesModel.swift
//  JeffPayfirmaExample
//
//  Created by Jeffrey Bakker on 2015-07-21.
//  Copyright (c) 2015 Jeffrey Bakker. All rights reserved.
//

import Foundation

// This Objective-C annotation will allow an object conforming to CharitiesModel
// to be injected via IBOutlet, as to separate the data model creation from the
// ViewController code.
@objc protocol CharitiesModel {

    func getAvailableCharitiesCount() -> Int
    func getCharityInfo(index : Int) -> CharityInfo
}