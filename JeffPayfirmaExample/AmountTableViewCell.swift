//
//  AmountTableViewCell.swift
//  JeffPayfirmaExample
//
//  Created by Jeffrey Bakker on 2015-07-21.
//  Copyright (c) 2015 Jeffrey Bakker. All rights reserved.
//

import UIKit

// Custom cell with UISegmentedControl inside
class AmountTableViewCell: UITableViewCell {

    let transparentColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.0)

    internal var amountChangedDelegate : AmountChangedListener?

    var segmented : UISegmentedControl?

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {

        super.init(style: style, reuseIdentifier: reuseIdentifier)

        self.backgroundColor = transparentColor

        var frame = CGRect(x: 2, y: 2, width: 370, height: 40)
        segmented = UISegmentedControl(frame: frame)
        segmented!.addTarget(self, action: "valueChanged:", forControlEvents: .ValueChanged);
        segmented!.insertSegmentWithTitle("$20", atIndex: 0, animated: false)
        segmented!.insertSegmentWithTitle("$50", atIndex: 1, animated: false)
        segmented!.insertSegmentWithTitle("$100", atIndex: 2, animated: false)
        segmented!.insertSegmentWithTitle("$250", atIndex: 3, animated: false)
        segmented!.selectedSegmentIndex = 0

        self.addSubview(segmented!)
        self.userInteractionEnabled = true
        segmented!.userInteractionEnabled = true

        self.backgroundView = nil
    }

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    func valueChanged(sender: AnyObject) {

        var selected = segmented!.selectedSegmentIndex
        var newAmount = segmented?.titleForSegmentAtIndex(selected)

        if amountChangedDelegate != nil {
            amountChangedDelegate?.amountChanged(newAmount!)
        }
    }

    func selectedAmount() -> String {

        var selected = segmented!.selectedSegmentIndex as Int!
        return segmented!.titleForSegmentAtIndex(selected)!
    }
}
