//
//  UrlDecoderTests.swift
//  JeffPayfirmaExampleTests
//
//  Created by Jeffrey Bakker on 2015-07-21.
//  Copyright (c) 2015 Jeffrey Bakker. All rights reserved.
//

import UIKit
import XCTest

class UrlDecoderTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testUrlDecoderDecodePairs() {

        // Arrange
        let firstKey = "firstKey"
        let firstVal = "firstVal"
        let secondKey = "secondKey"
        let secondVal = "secondVal"
        let thirdKey = "thirdKey"
        let thirdVal = "thirdVal"

        // Act
        var testUrl = "foo://bar.baz?firstKey=firstVal&secondKey=secondVal&thirdKey=thirdVal"
        var decoded = UrlDecoder.decodeUrl(NSURL(string: testUrl)!)

        // Assert
        XCTAssertEqual(decoded.count, 3, "Three items decoded")
        XCTAssertEqual(decoded[firstKey]!, firstVal, "First item decoded")
        XCTAssertEqual(decoded[secondKey]!, secondVal, "Second item decoded")
        XCTAssertEqual(decoded[thirdKey]!, thirdVal, "Third item decoded")
    }

    func testUrlDecoderNoValueForUnrecognizedKey() {

        // Arrange / Act
        let unknownKey = "fourthKey"
        var testUrl = "foo://bar.baz?firstKey=firstVal&secondKey=secondVal&thirdKey=thirdVal"
        var decoded = UrlDecoder.decodeUrl(NSURL(string: testUrl)!)

        // Assert
        XCTAssertNil(decoded[unknownKey], "Unknown key retuns nil")
    }

    func testUrlDecoderNoPairsInUrl() {

        // Arrange / Act
        let unknownKey = "fourthKey"
        var testUrl = "foo://bar.baz"
        var decoded = UrlDecoder.decodeUrl(NSURL(string: testUrl)!)

        // Assert
        XCTAssertEqual(decoded.count, 0, "No pairs in the URL")
    }
}
