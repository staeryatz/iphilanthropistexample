//
//  UrlGeneratorTests.swift
//  JeffPayfirmaExample
//
//  Created by Jeffrey Bakker on 2015-07-21.
//  Copyright (c) 2015 Jeffrey Bakker. All rights reserved.
//

import UIKit
import XCTest

class UrlGeneratorTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testExample() {

        // Arrange
        let expected = "payfirmahq://process?amount=20&transaction_type=SALE&description=Unicef&return_url=donate://openURL&email=canned@user.email"

        // Act
        let amount = "20"
        let charity = "Unicef"
        let actual = UrlGenerator.generateUrl(charity, amount: amount)

        // Assert
        XCTAssertEqual(actual, expected, "Generated URL is as expected.")
    }

    func testExampleEscaped1() {

        // Arrange
        let expected = "payfirmahq://process?amount=100&transaction_type=SALE&description=Salvation%20Army&return_url=donate://openURL&email=canned@user.email"

        // Act
        let amount = "100"
        let charity = "Salvation Army"
        let actual = UrlGenerator.generateUrl(charity, amount: amount)

        // Assert
        XCTAssertEqual(actual, expected, "Generated URL is as expected.")
    }

    func testExampleEscaped2() {

        // Arrange
        let expected = "payfirmahq://process?amount=250&transaction_type=SALE&description=BC%20Cancer%20Foundation&return_url=donate://openURL&email=canned@user.email"

        // Act
        let amount = "250"
        let charity = "BC Cancer Foundation"
        let actual = UrlGenerator.generateUrl(charity, amount: amount)

        // Assert
        XCTAssertEqual(actual, expected, "Generated URL is as expected.")
    }
}
